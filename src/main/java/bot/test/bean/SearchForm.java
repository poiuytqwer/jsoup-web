package bot.test.bean;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SearchForm {
	
private String startStation;	
private String endStation;
private Date searchDate;
private String searchTime;
private String searchWay;

}
