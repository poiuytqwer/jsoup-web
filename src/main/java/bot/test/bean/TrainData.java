package bot.test.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TrainData {

	private String no;	
	private String departure;
	private String arrival;
	private String duration;
	
	public TrainData(String no, String departure, String arrival, String duration) {
		super();
		this.no = no;
		this.departure = departure;
		this.arrival = arrival;
		this.duration = duration;
	}
	
}
