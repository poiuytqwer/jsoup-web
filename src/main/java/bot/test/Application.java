package bot.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**spring boot 初始化 ＆ 設定
 * spring boot可以參考https://spring.io/guides/gs/rest-service/
 * @author Connie
 * 
 */
@SpringBootApplication
public class Application{
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

} 