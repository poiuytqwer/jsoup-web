package bot.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import bot.test.bean.SearchForm;
import bot.test.bean.TrainData;
import bot.test.service.TestService;

/** 
 * 
 * @author Connie
 *
 */
@RestController
public class TestController {

	@Autowired
	private TestService testService;
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)//url path
	public List<TrainData> search(@RequestBody SearchForm searchForm) {
		return testService.search(searchForm); 
	}
}
