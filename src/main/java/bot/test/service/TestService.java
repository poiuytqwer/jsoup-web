package bot.test.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

import bot.test.bean.SearchForm;
import bot.test.bean.TrainData;

/**
 * @author Connie
 * jsoup 詳細用法參考http://ina-work.blogspot.tw/2016/10/java-jsoup-html.html
 */
@Service
public class TestService {

	public List<TrainData> search(SearchForm searchForm) {
		List<TrainData> trainDataList = null;
		Document doc = null;
		System.out.println(new SimpleDateFormat("yyyy/MM/dd").format(searchForm.getSearchDate()));
		try {
			doc = Jsoup.connect("http://www.thsrc.com.tw/en/TimeTable/SearchResult")//要爬頁面的url
					 .data("StartStation", searchForm.getStartStation())//需要傳送的form data
					 .data("EndStation", searchForm.getEndStation())
					 .data("SearchDate", new SimpleDateFormat("yyyy/MM/dd").format(searchForm.getSearchDate()))
					 .data("SearchTime", searchForm.getSearchTime())
					 .data("SearchWay", searchForm.getSearchWay()).post();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (doc != null) {                                         
			for (Element table : doc.select("table.touch_table")) {//找出結果對應的element,類似運用jquery的選擇器
				if (trainDataList == null) {
					trainDataList = new ArrayList<TrainData>();
				}
				TrainData train = new TrainData(table.select("td.column1").text(), 
												table.select("td.column3").text(),
												table.select("td.column4").text(), 
												table.select("td.column2").text());
				
				trainDataList.add(train);
			}
		}

		return trainDataList;
	}
}
